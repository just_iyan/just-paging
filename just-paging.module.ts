import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatSelectModule, MatProgressSpinnerModule, MatButtonModule } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import { JustPagingComponent } from './just-paging.component';

@NgModule({
  imports: [
    MatFormFieldModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    TranslateModule
  ],
  declarations: [JustPagingComponent],
  exports: [JustPagingComponent, FormsModule, ReactiveFormsModule]
})
export class JustPagingModule {}