import { Component, Input, OnInit, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'just-paging',
  templateUrl: './just-paging.component.html',
  styleUrls: ['./just-paging.scss']
})

export class JustPagingComponent implements OnInit {

  @Output() onChange: EventEmitter<any> = new EventEmitter();
  @Output() onNext: EventEmitter<number> = new EventEmitter();
  @Output() onPrev: EventEmitter<number> = new EventEmitter();

  _model: Array<any>;
  _pageSizeSelected: number;
  _pageNumber: number = 1;
  _next : boolean;
  _prev : boolean;
  _color : any;

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {
    this.changeDetectorRef.detectChanges();
  }

  onSelectedChange(e): void {
    console.log("onSelectedChange", e)
    this.onChange.emit(e.value);
  }

  previousPage(): void {
    this._pageNumber -= 1;
    console.log("this._pageNumber", this._pageNumber)
    this.onPrev.emit(this._pageNumber);
  }

  nextPage(): void {
    this._pageNumber = this._pageNumber + 1;
    console.log("this._pageNumber", this._pageNumber)
    this.onNext.emit(this._pageNumber);
  }



  @Input()
  set model(value: Array<any>) {
    this._model = value;
  }
  get model(): Array<any> {
    return this._model;
  }

  @Input()
  set pageSize(value: number) {
    this._pageSizeSelected = value;
  }
  get pageSize(): number {
    return this._pageSizeSelected;
  }

  @Input()
  set pageNumber(value: number) {
    this._pageNumber = value;
  }
  get pageNumber(): number {
    return this._pageNumber;
  }

  @Input()
  set disableNext(value: boolean){
    this._next = value;
  }
  get disableNext(): boolean{
    return this._next;
  }

  @Input()
  set disablePrev(value: boolean){
    this._prev = value;
  }
  get disablePrev(): boolean{
    return this._prev;
  }

  @Input()
  set color(value: any) {
    this._color = value;
  }
  get color(): any {
    return this._color;
  }


  constructor(private changeDetectorRef: ChangeDetectorRef) 
  {   
    this.model    = [];
  }
}